import * as actions from './index'

describe('actions dispatcher', () => {
    it('selectUser should dispatch SELECT_USER action', () => {
        expect(actions.selectUser(2))
            .toEqual({
                type: 'SELECT_USER',
                user: 2
            })
    })

    it('selectMonth should dispatch SELECT_MONTH action', () => {
        expect(actions.selectMonth(2))
            .toEqual({
                type: 'SELECT_MONTH',
                month: 2
            })
    })

    it('selectWeek should dispatch SELECT_WEEK action', () => {
        expect(actions.selectWeek(1))
            .toEqual({
                type: 'SELECT_WEEK',
                week: 1
            })
    })

    it('setMonthData should dispatch SET_MONTH_DATA action', () => {
        expect(actions.setMonthData([
				{ week_id: 1, week_number: 2 },
				{ week_id: 3, week_number: 4 }
		]))
        .toEqual({
            type: 'SET_MONTH_DATA',
            month_data: [
				{ week_id: 1, week_number: 2 },
				{ week_id: 3, week_number: 4 }
			]
        })
    })

    it('setWeekData should dispatch SET_WEEK_DATA action', () => {
        expect(actions.setWeekData({
			week_id: 1,
			week_number: 1,
			days_in_week: [],
			status: 'approved',
			owner_id: 1
		}))
        .toEqual({
            type: 'SET_WEEK_DATA',
            week_data: {
				week_id: 1,
				week_number: 1,
				days_in_week: [],
				status: 'approved',
				owner_id: 1
			}
        })
    })
})