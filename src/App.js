import React from 'react'
import { connect } from 'react-redux'

import SelectUser from './components/SelectUser'
import SelectMonth from './components/SelectMonth'
import GetMonthData from './components/GetMonthData'
import ViewMonthData from './components/ViewMonthData'
import ApproveWeek from './components/ApproveWeek'
import RejectWeek from './components/RejectWeek'

const App = ({ monthData }) => (
  <div className="App">
    <SelectUser />
    <SelectMonth />
    <GetMonthData />
    <ViewMonthData monthData={monthData} />
    {monthData.length > 0 ? <div><ApproveWeek /> <RejectWeek /></div> : ''}
  </div>
)

const mapStateToProps = state => ({ monthData: state.data.monthData })

export default connect(mapStateToProps)(App)
