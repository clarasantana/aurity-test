const initialState = {
    monthData: [],
    weekData: {}
}

const data = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_MONTH_DATA':
            return { ...state, monthData: action.month_data }
        case 'SET_WEEK_DATA':
            return { ...state, weekData: action.week_data } 
        default:
            return state
    }
}

export default data