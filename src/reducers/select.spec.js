import select from './select'

describe('select reducer', () => {
    it('should handle initial state', () => {
        expect(select(undefined, {}))
            .toEqual({ user: 1, month: 1, week: '' })
    })

    it('should handle SELECT_USER action', () => {
        expect(select({}, {
            type: 'SELECT_USER',
            user: 2
        })).toEqual({
            user: 2
        })
    })
    
    it('should handle SELECT_MONTH action', () => {
        expect(select({}, {
            type: 'SELECT_MONTH',
            month: 2
        })).toEqual({
            month: 2
        })
    })

    it('should handle SELECT_WEEK action', () => {
        expect(select({}, {
            type: 'SELECT_WEEK',
            week: 1
        })).toEqual({
            week: 1
        })
    })
})