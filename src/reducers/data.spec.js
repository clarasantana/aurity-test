import data from './data'

describe('data reducer', () => {
    it('should handle initial state', () => {
        expect(data(undefined, {}))
            .toEqual({ monthData: [], weekData: {} })
    })

    it('should handle SET_MONTH_DATA action', () => {
        expect(data({}, {
            type: 'SET_MONTH_DATA',
            month_data: {
				month: 1,
				year: 2017,
				weeks: [
					{ week_id: 1, week_number: 2 },
					{ week_id: 3, week_number: 4 }
				]
			}
        }))
        .toEqual({
            monthData: {
				month: 1,
				year: 2017,
				weeks: [
					{ week_id: 1, week_number: 2 },
					{ week_id: 3, week_number: 4 }
				]
			}
        })
    })

	it('should handle SET_WEEK_DATA action', () => {
		expect(data({}, {
			type: 'SET_WEEK_DATA',
			week_data: {
				week_id: 1,
				week_number: 1,
				days_in_week: [],
				status: 'approved',
				owner_id: 1
			}
		}))
		.toEqual({
			weekData: {
				week_id: 1,
				week_number: 1,
				days_in_week: [],
				status: 'approved',
				owner_id: 1
			}
		})
	})
})