const initialState = {
    user: 1,
    month: 1,
    week: ''
}

const select = (state = initialState, action) => {
    switch(action.type) {
        case 'SELECT_USER':
            return { ...state, user: action.user }
        case 'SELECT_MONTH':
            return { ...state, month: action.month } 
        case 'SELECT_WEEK':
            return { ...state, week: action.week } 
        default:
            return state
    }
}

export default select