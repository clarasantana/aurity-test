import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { selectWeek } from '../actions'
import { setWeekData } from '../actions'

import ViewWeekData from './ViewWeekData'

class ViewMonthData extends Component {
    constructor(props) {
        super(props)
        this.handleSelectWeek = this.handleSelectWeek.bind(this)
    }
    static propTypes = { monthData: PropTypes.array.isRequired }
    handleSelectWeek(e) {
        this.props.selectWeek(e.target.id)
        this.props.setWeekData(e.target.value)
    }

    render() {
        return(
            <div className="ViewMonthData">
                <ul>
                    {this.props.monthData.map(week =>
                        <li key={week.week_id}>
                            <input type="radio" name="week" id={week.week_id} value={week} onChange={this.handleSelectWeek} />
                            Week #{week.week_number}
                            <ViewWeekData weekData={week.days_in_week} />
                        </li>
                    )}
                </ul>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({ 
    selectWeek: selectWeek,
    setWeekData: setWeekData
 }, dispatch)
export default connect(null, mapDispatchToProps)(ViewMonthData)