import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'

class ApproveWeek extends Component {
    constructor(props) {
        super(props)
        this.handleApproveWeek = this.handleApproveWeek.bind(this)
    }
    handleApproveWeek(e) {
        axios.put('https://timesheet-staging-aurity.herokuapp.com/api/training/weeks/' + this.props.week + '/users/' + this.props.user, {
            status: 'approved'
        })
        .then(res => alert('Week succesfully approved!'))
        .catch(err => {
            alert('Ops, something went wrong while trying to approve the week')
            console.error(err)
        })
    }
    render() {
        return(
            <button onClick={this.handleApproveWeek}>APPROVE</button>
        )
    }
}

const mapStateToProps = state => ({
    user: state.select.user,
    week: state.select.week
})

export default connect(mapStateToProps)(ApproveWeek)