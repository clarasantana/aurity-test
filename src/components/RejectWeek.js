import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'

class RejectWeek extends Component {
    constructor(props) {
        super(props)
        this.handleRejectWeek = this.handleRejectWeek.bind(this)
    }
    handleRejectWeek(e) {
        axios.put('https://timesheet-staging-aurity.herokuapp.com/api/training/weeks/' + this.props.week + '/users/' + this.props.user, {
            status: 'rejected'
        })
        .then(res => alert('Week succesfully rejected!'))
        .catch(err => {
            alert('Ops, something went wrong while trying to reject the week')
            console.error(err)
        })
    }
    render() {
        return(
            <button onClick={this.handleRejectWeek}>REJECT</button>
        )
    }
}

const mapStateToProps = state => ({
    user: state.select.user,
    week: state.select.week
})

export default connect(mapStateToProps)(RejectWeek)