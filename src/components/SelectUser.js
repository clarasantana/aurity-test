import React, { Component } from 'react'
import axios from 'axios'

import SelectUserView from './SelectUserView'

export default class SelectUser extends Component {
    constructor(props) {
        super(props)
        this.state = { users: [] }
    }
    componentDidMount() {
        axios.get('https://timesheet-staging-aurity.herokuapp.com/api/users')
            .then(res => this.setState({ users: res.data }))
            .catch(err => {
                alert('Ops, something went wrong while trying to fetch the users')
                console.error(err)
            })
    }
    render() {
        return(
            <SelectUserView users={this.state.users} />
        )
    }
}