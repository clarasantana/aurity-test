import React, { Component } from 'react'
import axios from 'axios'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { setMonthData } from '../actions'

const compare = (a, b) =>  (a.week_number > b.week_number) ? 1 : ((b.week_number > b.week_number) ? - 1 : 0)

class GetMonthData extends Component {
    constructor(props) {
        super(props)
        this.handleGetMonthData = this.handleGetMonthData.bind(this)
    }
    handleGetMonthData(e) {
        axios.get('https://timesheet-staging-aurity.herokuapp.com/api/training/weeks/' + this.props.month + '/2017/' + this.props.user)
            .then(res => {
                const sorted = res.data.data.weeks.sort(compare)
                this.props.setMonthData(sorted)
            })
            .catch(err => {
                alert('Ops, something went wrong while trying to fetch the data')
                console.error(err)
            })
    }

    render() {
        return(
            <button onClick={this.handleGetMonthData}>SEARCH</button>
        )
    }
}

const mapStateToProps = state => ({ user: state.select.user, month: state.select.month })
const mapDispatchToProps = dispatch => bindActionCreators({ setMonthData: setMonthData}, dispatch)
export default connect (mapStateToProps, mapDispatchToProps)(GetMonthData)