import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { selectUser } from '../actions'

class SelectUserView extends Component {
    constructor(props) {
        super(props)
        this.handleSelectUser = this.handleSelectUser.bind(this)
    }
    static propTypes = { users: PropTypes.array.isRequired } 
    handleSelectUser(e) { this.props.selectUser(e.target.value) }

    render() {
        return(
            <select onChange={this.handleSelectUser}>
                {this.props.users.map(user => 
                    <option key={user.id} value={user.id}>{user.username} ({user.email})</option>
                )}
            </select>            
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({ selectUser: selectUser}, dispatch)

export default connect(null, mapDispatchToProps)(SelectUserView)