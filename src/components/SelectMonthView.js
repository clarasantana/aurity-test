import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { selectMonth } from '../actions'

class SelectMonthView extends Component {
    constructor(props) {
        super(props)
        this.handleSelectMonth = this.handleSelectMonth.bind(this)
    }
    static propTypes = { months: PropTypes.array.isRequired }
    handleSelectMonth(e) { this.props.selectMonth(e.target.value) }

    render() {
        return(
            <select onChange={this.handleSelectMonth}>
                {this.props.months.map(month =>
                    <option key={month.id} value={month.id}>{month.name} / 2017</option>
                )}
            </select>  
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({ selectMonth: selectMonth}, dispatch)
export default connect(null, mapDispatchToProps)(SelectMonthView)