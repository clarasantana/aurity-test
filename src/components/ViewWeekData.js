import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class ViewWeekData extends Component {
    static propTypes = { weekData : PropTypes.array.isRequired }
    
    render() {
        return(
            <ul className="ViewWeekData">
                {this.props.weekData.map(day =>
                    <li id={day.id}>
                        <h3>Day #{day.day_number}</h3>
                        <p>Worked: {day.hours} hours, {day.minutes} minutes</p>
                    </li>
                )}
            </ul>
        )
    }
}